// Copyright Epic Games, Inc. All Rights Reserved.


#include "Snake_GameGameModeBase.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "SnakePlayerController.h"


ASnake_GameGameModeBase::ASnake_GameGameModeBase()
{
	DefaultPawnClass = APlayerPawnBase::StaticClass();
	PlayerControllerClass = ASnakePlayerController::StaticClass();
}

void ASnake_GameGameModeBase::BeginPlay()
{
	Super::BeginPlay();

}

