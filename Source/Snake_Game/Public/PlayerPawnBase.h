// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Interactable.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKE_GAME_API APlayerPawnBase : public APawn, public IInteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(VisibleAnywhere)
	UCameraComponent* PawnCamera;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	int32 Lifes = 3;

	UPROPERTY(EditDefaultsOnly)
	int32 Score = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
	void CreateSnakeActor();

	void RemovePawn();

	UFUNCTION()
	void HandlePlayerVerticalInput(float Value);

	void HandlePlayerHorizontalInput(float Value);

	UFUNCTION(BlueprintCallable)
	void SetLifes(int32 Life);

	UFUNCTION(BlueprintPure)
	int32 GetLifes();

	UFUNCTION(BlueprintCallable)
	void SetScore(int32 Point);

	UFUNCTION(BlueprintPure)
	int32 GetScore();
	
};
