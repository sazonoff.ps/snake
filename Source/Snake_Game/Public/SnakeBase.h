// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

USTRUCT(BlueprintType)
struct FSnakeOptions
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize = 150.f;

	UPROPERTY(EditDefaultsOnly)
	int32 ElementsCount = 4;

	UPROPERTY(EditAnywhere)
	float MovementSpeed = 10.f;

	UPROPERTY(EditAnywhere)
	float MovementScaleCount = 0;

};

UCLASS()
class SNAKE_GAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Snake Options")
	FSnakeOptions SnakeOptions;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	bool bDie = false;

	
	bool bRotate;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	


	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int32 ElementsCount);

	UFUNCTION(BlueprintCallable)
	void RemoveSnakeElement();

	UFUNCTION(BlueprintCallable)
	void Move();

	

	void MoveSpeedUp(float Value);

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION(BlueprintPure)
	int32 GetDie();

	UFUNCTION(BlueprintImplementableEvent)
	void EatingSoundEffect();

	UFUNCTION(BlueprintImplementableEvent)
	void CrashedSoundEffect();

	UFUNCTION(BlueprintImplementableEvent)
	void CreateRIP(ASnakeElementBase* SnakeElementsIndex);

	
};
