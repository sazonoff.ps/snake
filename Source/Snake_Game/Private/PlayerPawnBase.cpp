// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"



// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>("PawnCamera");
	SetRootComponent(PawnCamera);


}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorLocation(FVector(0,0,2200));
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (PlayerInputComponent)
	{
		PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
		PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	}
}




void APlayerPawnBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
		this->RemovePawn();

	}
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}


void APlayerPawnBase::RemovePawn()
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->bDie == true)
		{
			CreateSnakeActor();
		}
		
	}
}

void APlayerPawnBase::HandlePlayerVerticalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
	
		if (Value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN && SnakeActor->bRotate == true)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;	
			SnakeActor->bRotate = false;
		}
		else if (Value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP && SnakeActor->bRotate == true)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			SnakeActor->bRotate = false;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		if (Value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT && SnakeActor->bRotate == true)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			SnakeActor->bRotate = false;
		}
		else if (Value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT && SnakeActor->bRotate == true)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			SnakeActor->bRotate = false;
		}
	}
}


void APlayerPawnBase::SetLifes(int32 Life)
{
	if (Lifes != 0)
	{
		Lifes -= Life;
	}
	
	
}

int32 APlayerPawnBase::GetLifes()
{
	return Lifes;
}

void APlayerPawnBase::SetScore(int32 Point)
{
	Score += Point;
}

int32 APlayerPawnBase::GetScore()
{
	return Score;
}

