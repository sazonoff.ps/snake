// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePlayerController.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"



void ASnakePlayerController::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerPawnBase::StaticClass(), Pawns);
}


