// Fill out your copyright notice in the Description page of Project Settings.


#include "Blockage.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"


// Sets default values
ABlockage::ABlockage()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BlockageMesh = CreateDefaultSubobject<UStaticMeshComponent>("Blockage");
	SetRootComponent(BlockageMesh);
}

// Called when the game starts or when spawned
void ABlockage::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockage::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		const auto Snake = Cast<ASnakeBase>(Interactor);
		APawn* PlayerPawns = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

		if (IsValid(Snake))
		{
			Snake->bDie = true;
			if (Cast<APlayerPawnBase>(PlayerPawns) != nullptr)
			{
				Cast<APlayerPawnBase>(PlayerPawns)->SetLifes(1);
				Cast<APlayerPawnBase>(PlayerPawns)->RemovePawn();

			}
			Snake->CrashedSoundEffect();
			Snake->RemoveSnakeElement();
			Snake->CreateRIP(Snake->SnakeElements[0]);
			Snake->SetLifeSpan(0.3f);
			
			
		}
	}
}

