// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FoodMesh = CreateDefaultSubobject<UStaticMeshComponent>("FoodMesh");
	SetRootComponent(FoodMesh);

	FVector Size = FVector(10.0f, 10.0f, 10.0f);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalRotation(FRotator(0, 100 * DeltaTime, 0));
	
	Movement();

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		APawn* PlayerPawns = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
			
		if (IsValid(Snake))
		{
			Snake->EatingSoundEffect();
			Snake->AddSnakeElement(1);
			Snake->MoveSpeedUp(0.1);
			this->Destroy();
			this->RandomSpawn();

			Cast<APlayerPawnBase>(PlayerPawns)->SetScore(1);
		}
	}
}

void AFood::RandomSpawn()
{

	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);

	while (SpawnY >= -250.f && SpawnY <= 270.f  && SpawnX >= -200.f && SpawnX <= 270.f)
	{
		SpawnY = FMath::FRandRange(MinY, MaxY);
		SpawnX = FMath::FRandRange(MinX, MaxX);
	}

	FVector FoodLocation = FVector(SpawnX, SpawnY, DefaultZ);
	const FTransform StartPoint = FTransform(FRotator::ZeroRotator, FoodLocation);

	if (GetWorld())
		{
			AFood* Food = GetWorld()->SpawnActor<AFood>(FoodActorClass, StartPoint);
		}
	
}

void AFood::Movement()
{
	FVector CurrentLocation = GetActorLocation();

	if (GetWorld())
	{
		float Time = GetWorld()->GetTimeSeconds();
		CurrentLocation.Z = CurrentLocation.Z + Amplitude * FMath::Sin(Frequency * Time);
		SetActorLocation(CurrentLocation);
	}
}

