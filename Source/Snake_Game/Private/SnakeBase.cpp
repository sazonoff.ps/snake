// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Interactable.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorTickInterval(SnakeOptions.MovementSpeed);

	AddSnakeElement(SnakeOptions.ElementsCount);


}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
	
}

void ASnakeBase::AddSnakeElement(int32 ElementsCount)
{
	for (int32 i = 0; i < ElementsCount; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * SnakeOptions.ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeOptions.SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);

		
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		NewSnakeElement->SetActorHiddenInGame(true);
		
	}
	
}

void ASnakeBase::RemoveSnakeElement()
{
	for (int32 i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		
		CurrentElement->Destroy();
		
	}
	//CreateRIP(SnakeElements[0]);
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

		switch (LastMoveDirection)
		{
		case EMovementDirection::UP: MovementVector.X += SnakeOptions.ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, 180, 0));
			bRotate = true;
			break;
		case EMovementDirection::DOWN: MovementVector.X -= SnakeOptions.ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, 0, 0));
			bRotate = true;
			break;
		case EMovementDirection::LEFT: MovementVector.Y += SnakeOptions.ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, -90, 0));
			bRotate = true;
			break;
		case EMovementDirection::RIGHT: MovementVector.Y -= SnakeOptions.ElementSize;
			SnakeElements[0]->SetActorRotation(FRotator(0, 90, 0));
			bRotate = true;
			break;
		default:
			break;
		}


	SnakeElements[0]->ToggleCollision();

	for (int32 i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i -1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		FRotator PrevRotation = PrevElement->GetActorRotation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorRotation(PrevRotation);
		CurrentElement->SetActorHiddenInGame(false);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->SetActorHiddenInGame(false);
	SnakeElements[0]->ToggleCollision();
}	


void ASnakeBase::MoveSpeedUp(float Value)
{
	
	SnakeOptions.MovementSpeed -= SnakeOptions.MovementSpeed * Value;
	if (SnakeOptions.MovementScaleCount < 5)
	{
		SetActorTickInterval(SnakeOptions.MovementSpeed);
		SnakeOptions.MovementScaleCount++;
	}
	else
	{
		SetActorTickInterval(SnakeOptions.MovementSpeed = 0.2);
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}


int32 ASnakeBase::GetDie()
{
  return bDie;
}









